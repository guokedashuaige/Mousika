
# tmux

参考资料：
[一文助你打通 tmux](https://zhuanlan.zhihu.com/p/102546608)

Linux 系统中通常使用 yum 来安装 tmux :
```
yum install tmux
```
Mac OS 通常使用 brew 来安装 tmux :
```
brew install tmux 
```

## tmux 中的几个名词:

session, window , pane 在这里我们就把他们分别叫做会话，窗口，窗格 。

## tmux 的前缀键

如果想使用 tmux 中的快捷键，我们必须使用 tmux 的前缀按键 ctrl + b , 在 tmux中所有的快捷键都需要通过前缀按键去唤起的。

## tmux 的 session 常用操作

### 新建 session:
```
tmux
```

上面的命令会创建一个 session ，默认是通过数字编号来命令的，有时候我们需要明确的知道我们的 session 的名字，方便我们快速进入该 session ,我们可以使用如下的命令：

```
tmux new -s  <session-name> 
```

### 离开 session

有时候我们需要离开终端，操作其他的任务，需要离开该任务，但是又想该任务继续在后台执行，这时候我们需要在 tmux 的任何一个窗格中输入如下命令：

```
tmux detach
```
也可以使用快捷键 `ctrl + b d` ,这里解释一下该快捷键， tmux 离开 session 的快捷键是 d ,但是在 tmux 当中任何快捷键都需要搭配 tmux 的前缀按键 ctrl + b 来唤醒，所以改快捷键的操作是先按 ctrl +b ,紧接着按下按键 d ,这样我们便顺利的离开当前 session 了。

### 查看 session 列表

查看我们目前操作了几个session：

```
tmux  ls 
```
也可以通过快捷键操作`ctrl + b s` 列出所有的 session。

### 进入 session

离开 session 之后，有时候我们还需要对某个 session 进行操作，这时候可以通过如下的操作：

```
tmux attach -t  <session-name>
```
或者 `tmux a -t <>`，该命令中的 a 是 attach 的简写形式

### 关闭 session

如果需要关闭 session, 可以通过执行如下的命令即可：

tmux kill-session -t <session-name>

也可以使用快捷键 `ctrl + d` 来关闭当前的 session。

### 切换 session

执行命令,可以从当前的 session 快速切换到另一个 session：

```
tmux switch -t <session-name>
```

### 重命名 session

```
tmux rename-session -t <old-session-name> <new-session-name>
```

### session 总结
session 在 tmux 操作当中非常重要，希望你可以熟练的使用以上操作：

新建 session -> 离开 session -> 查看 session 列表 -> 进入 session -> 关闭 session -> 不同 session 之间的切换 -> 重命名 session 。


## tmux 的窗格常用操作

什么是窗格（pane）呢？

前文也提到过，这里在详细描述一下：当前我们的工作区域，一块工作屏幕我们叫做窗口，窗口是可以被分割的，当前的工作区域被分割的一块块区域就是窗格。

每一个窗格我们可以用来干不同的事情，窗格同窗格之间是相互独立的，可以想象我们使用 vim 来搭配 tmux 的窗格功能是不是很酷呢？

### 切割窗格

切割窗格的命令是：
```
tmux split-window 
```
该命令会把当前工作区域分成上下两个小窗格

```
tmux split-window -h
```
该命令会把当前工作区域分成左右两个窗格

切割窗格的快捷键 `ctrl + b %` 可以快速的左右切割，`ctrl + b “ `可以快速的上下进行切割。

### 不同窗格间移动光标
```
tmux select-pane  -U
```
把当前光标移动到上方的窗格

```
tmux select-pane -D
```
把当前的光标移动的下方的窗格

```
tmux select-pane -L
```
把当前的光标移动到左边的窗格

```
tmux select-pane -R
```
把当前的光标移动到右边的窗格

移动窗格光标的快捷键：

`ctrl +b <arrow key>`例如 `ctrl +b ⬆` 会把光标移动到上方的窗格。

`ctrl +b ;`光标切换到上一个窗格

`ctrl +b o` 光标切换到下一个窗格

### 交换窗格的位置
```
tmux swap-pane -U
```
当前窗格向上移动
```
tmux swap-pane -D
```
当前窗格向下移动


### 关闭当前的窗格

关闭窗格通常使用快捷键 `ctrl + b x`

### 放大窗格

快捷键 `ctrl + b z` ,将会放大当前操作的窗格，继续触发该快捷键将会还原当前的窗格。


### 窗格显示时间

快捷键 `ctrl +b t` 将会把在当前的窗格当中显示时钟，非常酷炫的一个功能，点击 `enter` (回车键将会复原)。

### 窗格总结

关于窗格的操作我们经常操作的就是分割，移动光标，放大窗格，关闭窗格，可以熟练以上提到的操作，关于移动光标的快捷键操作，下文在 .tmux.conf 中也会处理成快捷键进行操作。


## tmux 的窗口常用操作

### 创建窗口

有时候一个窗口不够用，这样我们就需要重新创建一个窗口：

```
tmux new-window -n <window-name>
```

创建窗口的快捷键`ctrl + b c`, 可以通过快捷键快速的创建一个窗口出来。

### 切换窗口
```
tmux select-window -t <window-name>
```

关于切换窗口的快捷键 :

`ctrl + b w` 显示窗口列表可以通过 j ,k 上下进行选择窗口，然后回车进入指定的窗口。

`ctrl + b n` 快速切换到下一个窗口。

`ctrl +b p` 快速切换到上一个窗口。


### 重命名窗口
```
tmux rename-window <new-window-name>
```

### 关闭窗口

tmux kill-window -t <window-name>

关闭窗口的快捷键：

`ctrl + b & `可以关闭当前的的窗口

### 滚动窗口


`Ctrl- b [`您可以使用普通的导航键滚动（例如Up Arrow或PgDn）。按q退出滚动模式。

或者，您可以按`Ctrl- b PgUp`直接进入复印模式并向上滚动一页（这听起来似乎是您大多数时候都想要的）

在vi模式下（请参阅下文），您还可以使用`Shift- k`和`Shift- j`（如果您已经处于滚动模式）逐行向上/向下滚动页面。光标不移动，而不移动页面。

通过使用前缀键（默认情况下为'C-b'（Ctrl-b））和命令键的组合键，可以从附加的客户端控制tmux。
```
 The default command key bindings are:

[           Enter copy mode to copy text or view the history.

Function                     vi              emacs
--------                     --              -----
Half page down               C-d             M-Down
Half page up                 C-u             M-Up
Next page                    C-f             Page down
Previous page                C-b             Page up
Scroll down                  C-Down or C-e   C-Down
Scroll up                    C-Up or C-y     C-Up
Search again                 n               n
Search again in reverse      N               N
Search backward              ?               C-r
Search forward               /               C-s
```

更多资料：
[tmux美化（基于oh-my-tmux)](https://zhuanlan.zhihu.com/p/11242684n)
[如何滚动tmux？](https://qastack.cn/superuser/209437/how-do-i-scroll-in-tmux)
[.tmux](https://github.com/gpakosz/.tmux)