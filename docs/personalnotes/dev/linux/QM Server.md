# QM Server

# 登录服务器

在服务器上创建自己的用户，然后远端ssh登录

IP:192.168.4.18

# 安装oh my zsh

*   配置git忽略根证书

```bash
$ git config --global http.sslVerify false
```

安装 oh-my-zsh,使用curl -k 忽略整数

```bash
sh -c "$(curl -k -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

*   配置皮肤

进入：[https://github.com/ohmyzsh/ohmyzsh/wiki/Themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes "https://github.com/ohmyzsh/ohmyzsh/wiki/Themes") ，选择一个喜欢的皮肤，在`~/.zshrc`中修改

![](image/image_gARLHtnuvw.png)

*   配置自动登录ZSH

```bash
source $ZSH/oh-my-zsh.sh
```

*   配置插件

```bash
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
```

*   配置Tmux自动attach

```bash
#TMUX
if which tmux >/dev/null 2>&1; then
   test -z "$TMUX" && (tmux attach || tmux new-session)
fi
```

*   使用`source ~/.zshrc`使配置生效

# 下载 oh-my-tmux

```bash
$ cd
$ git clone https://github.com/gpakosz/.tmux.git
$ ln -s -f .tmux/.tmux.conf
$ cp .tmux/.tmux.conf.local .
```

# 下载QMStudio配置文件

```bash
$ git clone https://github.com/quintin-lee/NVCode.git ~/.config/nvim

#运行Packer，安装插件
$ nvim +PackerSync
```

LSP语法增强遇到问题无法安装，可以直接从琦敏的用户拷贝

```bash
#切换到root 密码空格
$ su

$ sudo cp /home/liqm/.local/share/nvim/site/pack/packer/start/nvim-treesitter/parser/* /home/`whoami`/.local/share/nvim/site/pack/packer/start/nvim-treesitter/parser

$ vim
```

LSP服务器无法正常安装，从琦敏的目录拷贝

```bash
#切换到root 密码空格
$ su

$ sudo cp -r /home/liqm/.local/share/nvim/lsp_servers/* /home/`whoami`/.local/share/nvim/lsp_servers

$ vim
```

vim中使用LspInstallInfo查看安装过程，全部打钩为通过

完成图

![](image/image_pK3T7cIgjk.png)
