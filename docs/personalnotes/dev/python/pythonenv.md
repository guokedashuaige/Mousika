# python用env
创建环境
```
python3 -m venv tutorial-env
```
启动环境
```
source tutorial-env/bin/activate
```

用pip管理包
```
 python -m pip install
```
 
查看包： `pip show` ，`pip list`

pip freeze will produce a similar list of the installed packages, but the output uses the format that pip install expects. A common convention is to put this list in a requirements.txt file:

```
pip freeze > requirements.txt
```

The requirements.txt can then be committed to version control and shipped as part of an application. Users can then install all the necessary packages with install -r:
```
 python -m pip install -r requirements.txt
```

退出：
` deactivate `